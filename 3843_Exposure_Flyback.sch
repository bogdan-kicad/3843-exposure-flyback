EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_01x02_Male J3
U 1 1 5DB7FDA9
P 14850 4050
F 0 "J3" H 14822 3932 50  0000 R CNN
F 1 "12V" H 14822 4023 50  0000 R CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 14850 4050 50  0001 C CNN
F 3 "~" H 14850 4050 50  0001 C CNN
	1    14850 4050
	-1   0    0    1   
$EndComp
$Comp
L Device:D_Schottky D3
U 1 1 5DB872C0
P 12350 3950
F 0 "D3" H 12350 3734 50  0000 C CNN
F 1 "1N5822" H 12350 3825 50  0000 C CNN
F 2 "Diode_THT:D_DO-15_P10.16mm_Horizontal" H 12350 3950 50  0001 C CNN
F 3 "~" H 12350 3950 50  0001 C CNN
	1    12350 3950
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5DB8A1CB
P 14650 4050
F 0 "#PWR0102" H 14650 3800 50  0001 C CNN
F 1 "GND" H 14655 3877 50  0000 C CNN
F 2 "" H 14650 4050 50  0001 C CNN
F 3 "" H 14650 4050 50  0001 C CNN
	1    14650 4050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 5DB8BE3B
P 12100 4650
F 0 "#PWR0103" H 12100 4400 50  0001 C CNN
F 1 "GND" H 12105 4477 50  0000 C CNN
F 2 "" H 12100 4650 50  0001 C CNN
F 3 "" H 12100 4650 50  0001 C CNN
	1    12100 4650
	1    0    0    -1  
$EndComp
$Comp
L Isolator:PC817 U1
U 1 1 5DB92028
P 11900 7400
F 0 "U1" H 11900 7725 50  0000 C CNN
F 1 "PC817" H 11900 7634 50  0000 C CNN
F 2 "Package_DIP:DIP-4_W7.62mm" H 11700 7200 50  0001 L CIN
F 3 "http://www.soselectronic.cz/a_info/resource/d/pc817.pdf" H 11900 7400 50  0001 L CNN
	1    11900 7400
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5DB92CEB
P 12350 7850
F 0 "R3" H 12420 7896 50  0000 L CNN
F 1 "1k" H 12420 7805 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 12280 7850 50  0001 C CNN
F 3 "~" H 12350 7850 50  0001 C CNN
	1    12350 7850
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_GDS Q1
U 1 1 5DB96210
P 11000 5200
F 0 "Q1" H 11206 5246 50  0000 L CNN
F 1 "IRF840" H 11206 5155 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 11200 5300 50  0001 C CNN
F 3 "~" H 11000 5200 50  0001 C CNN
	1    11000 5200
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR0107
U 1 1 5DB96ED3
P 11100 6050
F 0 "#PWR0107" H 11100 5800 50  0001 C CNN
F 1 "GNDA" H 11105 5877 50  0000 C CNN
F 2 "" H 11100 6050 50  0001 C CNN
F 3 "" H 11100 6050 50  0001 C CNN
	1    11100 6050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5DB98099
P 11100 5850
F 0 "R2" H 11170 5896 50  0000 L CNN
F 1 "0.47/3W" H 11170 5805 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0411_L9.9mm_D3.6mm_P5.08mm_Vertical" V 11030 5850 50  0001 C CNN
F 3 "~" H 11100 5850 50  0001 C CNN
	1    11100 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	11100 6050 11100 6000
Wire Wire Line
	11100 4150 11550 4150
$Comp
L Device:CP C1
U 1 1 5DBA5EB7
P 10150 3300
F 0 "C1" H 10268 3346 50  0000 L CNN
F 1 "100u/400V" H 10000 3650 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D16.0mm_P7.50mm" H 10188 3150 50  0001 C CNN
F 3 "~" H 10150 3300 50  0001 C CNN
	1    10150 3300
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR0109
U 1 1 5DBA6C61
P 10150 3450
F 0 "#PWR0109" H 10150 3200 50  0001 C CNN
F 1 "GNDA" H 10155 3277 50  0000 C CNN
F 2 "" H 10150 3450 50  0001 C CNN
F 3 "" H 10150 3450 50  0001 C CNN
	1    10150 3450
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Bridge_+-AA D1
U 1 1 5DBA8516
P 9600 3050
F 0 "D1" H 9550 3550 50  0000 L CNN
F 1 "D_Bridge_+-AA" H 9944 3005 50  0001 L CNN
F 2 "Diode_THT:Diode_Bridge_DIP-4_W7.62mm_P5.08mm" H 9600 3050 50  0001 C CNN
F 3 "~" H 9600 3050 50  0001 C CNN
	1    9600 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	9900 3050 9950 3050
$Comp
L power:GNDA #PWR0110
U 1 1 5DBA9B88
P 9150 3150
F 0 "#PWR0110" H 9150 2900 50  0001 C CNN
F 1 "GNDA" H 9155 2977 50  0000 C CNN
F 2 "" H 9150 3150 50  0001 C CNN
F 3 "" H 9150 3150 50  0001 C CNN
	1    9150 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	9150 3150 9150 3050
Wire Wire Line
	9150 3050 9300 3050
$Comp
L Connector:Conn_01x02_Male J1
U 1 1 5DBAADCC
P 8650 2950
F 0 "J1" H 8758 3131 50  0000 C CNN
F 1 "AC_IN" H 8758 3040 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 8650 2950 50  0001 C CNN
F 3 "~" H 8650 2950 50  0001 C CNN
	1    8650 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	8850 2950 9000 2950
Wire Wire Line
	9000 2950 9000 2650
Wire Wire Line
	9600 2650 9600 2750
Wire Wire Line
	8850 3050 9000 3050
Wire Wire Line
	9000 3050 9000 3450
Wire Wire Line
	9000 3450 9600 3450
Wire Wire Line
	9600 3450 9600 3350
$Comp
L Device:D_Schottky D2
U 1 1 5DBB1A0C
P 10500 4350
F 0 "D2" H 10500 4474 50  0000 C CNN
F 1 "1N5822" H 10500 4250 50  0000 C CNN
F 2 "Diode_THT:D_T-1_P5.08mm_Horizontal" H 10500 4350 50  0001 C CNN
F 3 "~" H 10500 4350 50  0001 C CNN
	1    10500 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5DBCBC05
P 9950 3950
F 0 "R1" H 9880 3904 50  0000 R CNN
F 1 "47k/2W" H 9880 3995 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0411_L9.9mm_D3.6mm_P5.08mm_Vertical" V 9880 3950 50  0001 C CNN
F 3 "~" H 9950 3950 50  0001 C CNN
	1    9950 3950
	-1   0    0    1   
$EndComp
Wire Wire Line
	11100 4150 11100 5000
$Comp
L power:GNDA #PWR0111
U 1 1 5DBD4E02
P 11500 4650
F 0 "#PWR0111" H 11500 4400 50  0001 C CNN
F 1 "GNDA" H 11505 4477 50  0000 C CNN
F 2 "" H 11500 4650 50  0001 C CNN
F 3 "" H 11500 4650 50  0001 C CNN
	1    11500 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Zener D6
U 1 1 5DBFE275
P 11100 3400
F 0 "D6" V 11146 3321 50  0000 R CNN
F 1 "TVS" V 11055 3321 50  0000 R CNN
F 2 "Diode_THT:D_T-1_P10.16mm_Horizontal" H 11100 3400 50  0001 C CNN
F 3 "~" H 11100 3400 50  0001 C CNN
	1    11100 3400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	11100 4000 11100 4150
Connection ~ 11100 4150
Wire Wire Line
	11100 3700 11100 3650
Wire Wire Line
	11100 3250 11100 3050
$Comp
L Device:R R4
U 1 1 5EB28C99
P 10800 4350
F 0 "R4" V 10900 4300 50  0000 L CNN
F 1 "22" V 10800 4300 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 10730 4350 50  0001 C CNN
F 3 "~" H 10800 4350 50  0001 C CNN
	1    10800 4350
	0    1    1    0   
$EndComp
Wire Wire Line
	11550 3950 11550 3050
Wire Wire Line
	11550 3050 11100 3050
Connection ~ 11100 3050
Wire Wire Line
	13550 4000 13550 3950
Wire Wire Line
	13550 3950 14000 3950
Connection ~ 14350 3950
Wire Wire Line
	14350 3950 14650 3950
$Comp
L Device:D_Schottky D4
U 1 1 5EB50455
P 11100 3850
F 0 "D4" V 11054 3929 50  0000 L CNN
F 1 "UF4007" V 11145 3929 50  0000 L CNN
F 2 "Diode_THT:D_T-1_P10.16mm_Horizontal" H 11100 3850 50  0001 C CNN
F 3 "~" H 11100 3850 50  0001 C CNN
	1    11100 3850
	0    1    1    0   
$EndComp
$Comp
L Device:LED D5
U 1 1 5EB62E6E
P 14650 4750
F 0 "D5" H 14643 4495 50  0000 C CNN
F 1 "LED" H 14643 4586 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm" H 14650 4750 50  0001 C CNN
F 3 "~" H 14650 4750 50  0001 C CNN
	1    14650 4750
	-1   0    0    1   
$EndComp
$Comp
L Device:R R9
U 1 1 5EB6394F
P 14950 4950
F 0 "R9" H 15020 4996 50  0000 L CNN
F 1 "560" H 15020 4905 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 14880 4950 50  0001 C CNN
F 3 "~" H 14950 4950 50  0001 C CNN
	1    14950 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	14500 4750 14350 4750
Connection ~ 14350 4750
Wire Wire Line
	14350 4750 14350 3950
Wire Wire Line
	14800 4750 14950 4750
Wire Wire Line
	14950 4750 14950 4800
$Comp
L power:GND #PWR0112
U 1 1 5EB688A0
P 14950 5200
F 0 "#PWR0112" H 14950 4950 50  0001 C CNN
F 1 "GND" H 14955 5027 50  0000 C CNN
F 2 "" H 14950 5200 50  0001 C CNN
F 3 "" H 14950 5200 50  0001 C CNN
	1    14950 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	14950 5200 14950 5100
$Comp
L Connector:Conn_01x07_Male J4
U 1 1 5EE07A20
P 11750 4250
F 0 "J4" H 11722 4274 50  0001 R CNN
F 1 "PRI" H 11722 4183 50  0001 R CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MKDS-1,5-7-5.08_1x07_P5.08mm_Horizontal" H 11750 4250 50  0001 C CNN
F 3 "~" H 11750 4250 50  0001 C CNN
	1    11750 4250
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x07_Male J5
U 1 1 5EE07F33
P 11800 4250
F 0 "J5" H 11772 4274 50  0001 R CNN
F 1 "SEC" H 11772 4183 50  0001 R CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MKDS-1,5-7-5.08_1x07_P5.08mm_Horizontal" H 11800 4250 50  0001 C CNN
F 3 "~" H 11800 4250 50  0001 C CNN
	1    11800 4250
	1    0    0    1   
$EndComp
Wire Wire Line
	12100 4650 12100 4550
Wire Wire Line
	12100 4550 12000 4550
Wire Wire Line
	12200 3950 12100 3950
Wire Wire Line
	12000 4050 12100 4050
Wire Wire Line
	12100 4050 12100 3950
Connection ~ 12100 3950
Wire Wire Line
	12100 3950 12000 3950
Wire Wire Line
	9950 3800 9950 3050
Connection ~ 9950 3050
Wire Wire Line
	9950 4100 9950 4350
Wire Wire Line
	9950 4350 10350 4350
Wire Wire Line
	9950 3050 10150 3050
Wire Wire Line
	10150 3150 10150 3050
Connection ~ 10150 3050
Wire Wire Line
	10150 3050 10500 3050
$Comp
L Device:C C5
U 1 1 5EB4464E
P 10800 3400
F 0 "C5" H 10850 3500 50  0000 L CNN
F 1 "10n/1kV" H 10650 3850 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P5.00mm" H 10838 3250 50  0001 C CNN
F 3 "~" H 10800 3400 50  0001 C CNN
	1    10800 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	10800 3550 10800 3650
Wire Wire Line
	10800 3650 11100 3650
Connection ~ 11100 3650
Wire Wire Line
	11100 3650 11100 3550
Wire Wire Line
	10800 3250 10800 3050
Connection ~ 10800 3050
Wire Wire Line
	10800 3050 11100 3050
$Comp
L Device:R R10
U 1 1 5EB4A4FA
P 10500 3400
F 0 "R10" H 10550 3650 50  0000 L CNN
F 1 "47k/2W" H 10400 3050 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0411_L9.9mm_D3.6mm_P5.08mm_Vertical" V 10430 3400 50  0001 C CNN
F 3 "~" H 10500 3400 50  0001 C CNN
	1    10500 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	10500 3250 10500 3050
Connection ~ 10500 3050
Wire Wire Line
	10500 3050 10800 3050
Wire Wire Line
	10500 3550 10500 3650
Wire Wire Line
	10500 3650 10800 3650
Connection ~ 10800 3650
$Comp
L Device:Fuse F1
U 1 1 5EB78994
P 9300 2650
F 0 "F1" V 9103 2650 50  0000 C CNN
F 1 "Fuse" V 9194 2650 50  0000 C CNN
F 2 "Fuse:Fuse_Littelfuse_395Series" V 9230 2650 50  0001 C CNN
F 3 "~" H 9300 2650 50  0001 C CNN
	1    9300 2650
	0    1    1    0   
$EndComp
Wire Wire Line
	9150 2650 9000 2650
Wire Wire Line
	9450 2650 9600 2650
Text Notes 11850 3400 0    50   ~ 0
54.5kHz\n134T-5T-5T
$Comp
L power:GND #PWR0106
U 1 1 5EB48235
P 13550 4300
F 0 "#PWR0106" H 13550 4050 50  0001 C CNN
F 1 "GND" H 13555 4127 50  0001 C CNN
F 2 "" H 13550 4300 50  0001 C CNN
F 3 "" H 13550 4300 50  0001 C CNN
	1    13550 4300
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C4
U 1 1 5EB4822F
P 13550 4150
F 0 "C4" H 13500 3700 50  0000 L CNN
F 1 "1000u/25V" H 13300 3800 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D10.0mm_P5.00mm" H 13588 4000 50  0001 C CNN
F 3 "~" H 13550 4150 50  0001 C CNN
	1    13550 4150
	1    0    0    -1  
$EndComp
$Comp
L pspice:INDUCTOR L1
U 1 1 5F60FB31
P 13000 3950
F 0 "L1" H 13000 4165 50  0000 C CNN
F 1 "3.3uH" H 13000 4074 50  0000 C CNN
F 2 "Inductor_THT:L_Toroid_Vertical_L16.8mm_W9.2mm_P7.10mm_Vishay_TJ3" H 13000 3950 50  0001 C CNN
F 3 "~" H 13000 3950 50  0001 C CNN
	1    13000 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	12750 3950 12500 3950
Wire Wire Line
	13250 3950 13550 3950
Connection ~ 13550 3950
Wire Wire Line
	11550 4450 11500 4450
Wire Wire Line
	10950 4350 11550 4350
Wire Wire Line
	11500 4450 11500 4650
Wire Wire Line
	12000 4450 12100 4450
Wire Wire Line
	12100 4450 12100 4550
Connection ~ 12100 4550
Wire Wire Line
	9750 5200 10600 5200
$Comp
L Regulator_Controller:UC3843_SOIC8 U3
U 1 1 5F618FC5
P 8100 5400
F 0 "U3" H 8400 5950 50  0000 C CNN
F 1 "UC3843_SOIC8" H 8400 5850 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 8100 4950 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/uc3842.pdf" H 8100 5400 50  0001 C CNN
	1    8100 5400
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR0101
U 1 1 5F618FCB
P 8100 5800
F 0 "#PWR0101" H 8100 5550 50  0001 C CNN
F 1 "GNDA" H 8105 5627 50  0000 C CNN
F 2 "" H 8100 5800 50  0001 C CNN
F 3 "" H 8100 5800 50  0001 C CNN
	1    8100 5800
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C6
U 1 1 5F618FD1
P 7100 5100
F 0 "C6" H 7192 5146 50  0000 L CNN
F 1 "10n" H 7192 5055 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 7100 5100 50  0001 C CNN
F 3 "~" H 7100 5100 50  0001 C CNN
	1    7100 5100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R11
U 1 1 5F618FD7
P 6800 5100
F 0 "R11" H 6870 5146 50  0000 L CNN
F 1 "10k" H 6870 5055 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 6730 5100 50  0001 C CNN
F 3 "~" H 6800 5100 50  0001 C CNN
	1    6800 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6800 4950 6800 4900
Wire Wire Line
	6800 4900 7100 4900
Wire Wire Line
	7100 4900 7100 5000
Wire Wire Line
	6800 5250 6800 5300
Wire Wire Line
	6800 5300 7100 5300
Wire Wire Line
	7100 5300 7100 5200
Wire Wire Line
	7100 5300 7600 5300
Connection ~ 7100 5300
Wire Wire Line
	7600 5200 7450 5200
Wire Wire Line
	7450 5200 7450 4900
Wire Wire Line
	7450 4900 7100 4900
Connection ~ 7100 4900
$Comp
L Device:C_Small C2
U 1 1 5F618FE9
P 6850 5850
F 0 "C2" H 6650 5900 50  0000 L CNN
F 1 "100n" H 6600 5800 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6850 5850 50  0001 C CNN
F 3 "~" H 6850 5850 50  0001 C CNN
	1    6850 5850
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C7
U 1 1 5F618FEF
P 7350 5850
F 0 "C7" H 7442 5896 50  0000 L CNN
F 1 "3.3n" H 7442 5805 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 7350 5850 50  0001 C CNN
F 3 "~" H 7350 5850 50  0001 C CNN
	1    7350 5850
	1    0    0    -1  
$EndComp
$Comp
L Device:R R12
U 1 1 5F618FF5
P 7100 5600
F 0 "R12" V 7300 5600 50  0000 C CNN
F 1 "10k" V 7200 5600 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 7030 5600 50  0001 C CNN
F 3 "~" H 7100 5600 50  0001 C CNN
	1    7100 5600
	0    1    1    0   
$EndComp
Wire Wire Line
	7350 5750 7350 5600
Wire Wire Line
	7350 5600 7600 5600
Wire Wire Line
	7350 5600 7250 5600
Connection ~ 7350 5600
Wire Wire Line
	6850 5750 6850 5600
Wire Wire Line
	6850 5600 6950 5600
$Comp
L power:GNDA #PWR0108
U 1 1 5F619001
P 7350 5950
F 0 "#PWR0108" H 7350 5700 50  0001 C CNN
F 1 "GNDA" H 7355 5777 50  0000 C CNN
F 2 "" H 7350 5950 50  0001 C CNN
F 3 "" H 7350 5950 50  0001 C CNN
	1    7350 5950
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR0113
U 1 1 5F619007
P 6850 5950
F 0 "#PWR0113" H 6850 5700 50  0001 C CNN
F 1 "GNDA" H 6855 5777 50  0000 C CNN
F 2 "" H 6850 5950 50  0001 C CNN
F 3 "" H 6850 5950 50  0001 C CNN
	1    6850 5950
	1    0    0    -1  
$EndComp
Connection ~ 6850 5600
Wire Wire Line
	6800 5300 6400 5300
Connection ~ 6800 5300
Wire Wire Line
	6500 5600 6850 5600
Wire Wire Line
	6850 5600 6850 5500
Wire Wire Line
	6850 5500 7600 5500
$Comp
L Device:R R13
U 1 1 5F619013
P 9600 5200
F 0 "R13" V 9500 5150 50  0000 L CNN
F 1 "22" V 9700 5150 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 9530 5200 50  0001 C CNN
F 3 "~" H 9600 5200 50  0001 C CNN
	1    9600 5200
	0    1    1    0   
$EndComp
Wire Wire Line
	8100 4350 8100 5000
$Comp
L Device:C_Small C10
U 1 1 5F61901A
P 9100 5850
F 0 "C10" H 9192 5896 50  0000 L CNN
F 1 "470p" H 9192 5805 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 9100 5850 50  0001 C CNN
F 3 "~" H 9100 5850 50  0001 C CNN
	1    9100 5850
	1    0    0    -1  
$EndComp
$Comp
L Device:R R14
U 1 1 5F619020
P 9600 5600
F 0 "R14" V 9800 5600 50  0000 C CNN
F 1 "1k" V 9700 5600 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 9530 5600 50  0001 C CNN
F 3 "~" H 9600 5600 50  0001 C CNN
	1    9600 5600
	0    1    1    0   
$EndComp
Wire Wire Line
	9450 5600 9100 5600
Wire Wire Line
	9100 5750 9100 5600
Connection ~ 9100 5600
Wire Wire Line
	9100 5600 8600 5600
$Comp
L power:GNDA #PWR0114
U 1 1 5F61902A
P 9100 5950
F 0 "#PWR0114" H 9100 5700 50  0001 C CNN
F 1 "GNDA" H 9105 5777 50  0000 C CNN
F 2 "" H 9100 5950 50  0001 C CNN
F 3 "" H 9100 5950 50  0001 C CNN
	1    9100 5950
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C9
U 1 1 5F619030
P 7900 4550
F 0 "C9" H 7700 4600 50  0000 L CNN
F 1 "100n" H 7650 4500 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 7900 4550 50  0001 C CNN
F 3 "~" H 7900 4550 50  0001 C CNN
	1    7900 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 4450 7900 4350
Wire Wire Line
	7900 4350 8100 4350
Connection ~ 8100 4350
$Comp
L power:GNDA #PWR0115
U 1 1 5F619039
P 7900 4650
F 0 "#PWR0115" H 7900 4400 50  0001 C CNN
F 1 "GNDA" H 7905 4477 50  0000 C CNN
F 2 "" H 7900 4650 50  0001 C CNN
F 3 "" H 7900 4650 50  0001 C CNN
	1    7900 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	9750 5600 11100 5600
$Comp
L Device:CP_Small C8
U 1 1 5F619042
P 7500 4550
F 0 "C8" H 7450 4900 50  0000 L CNN
F 1 "100u/25V" H 7300 4800 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 7500 4550 50  0001 C CNN
F 3 "~" H 7500 4550 50  0001 C CNN
	1    7500 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 4450 7500 4350
Wire Wire Line
	7500 4350 7900 4350
Connection ~ 7900 4350
$Comp
L power:GNDA #PWR0116
U 1 1 5F61904B
P 7500 4650
F 0 "#PWR0116" H 7500 4400 50  0001 C CNN
F 1 "GNDA" H 7505 4477 50  0000 C CNN
F 2 "" H 7500 4650 50  0001 C CNN
F 3 "" H 7500 4650 50  0001 C CNN
	1    7500 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	8600 5200 9450 5200
Wire Wire Line
	8100 4350 9350 4350
$Comp
L Device:R R15
U 1 1 5F61D86E
P 10600 5900
AR Path="/5F61D86E" Ref="R15"  Part="1" 
AR Path="/5F60CAE8/5F61D86E" Ref="R?"  Part="1" 
F 0 "R15" H 10670 5946 50  0000 L CNN
F 1 "22k" H 10670 5855 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 10530 5900 50  0001 C CNN
F 3 "~" H 10600 5900 50  0001 C CNN
	1    10600 5900
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR0117
U 1 1 5F61D874
P 10600 6050
AR Path="/5F61D874" Ref="#PWR0117"  Part="1" 
AR Path="/5F60CAE8/5F61D874" Ref="#PWR?"  Part="1" 
F 0 "#PWR0117" H 10600 5800 50  0001 C CNN
F 1 "GNDA" H 10605 5877 50  0000 C CNN
F 2 "" H 10600 6050 50  0001 C CNN
F 3 "" H 10600 6050 50  0001 C CNN
	1    10600 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	10600 5750 10600 5200
Connection ~ 9950 4350
Wire Wire Line
	10600 5200 10800 5200
Connection ~ 10600 5200
Wire Wire Line
	11100 5700 11100 5600
Wire Wire Line
	11100 5400 11100 5600
Connection ~ 11100 5600
Wire Wire Line
	11600 7300 6500 7300
Wire Wire Line
	6500 5600 6500 7300
Wire Wire Line
	6400 7500 11600 7500
Wire Wire Line
	6400 5300 6400 7500
Text Label 7000 5500 0    50   ~ 0
VREF
$Comp
L Device:D_Zener D8
U 1 1 5F64A62F
P 13000 7300
F 0 "D8" H 13000 7083 50  0000 C CNN
F 1 "12V" H 13000 7174 50  0000 C CNN
F 2 "Diode_THT:D_T-1_P10.16mm_Horizontal" H 13000 7300 50  0001 C CNN
F 3 "~" H 13000 7300 50  0001 C CNN
	1    13000 7300
	-1   0    0    1   
$EndComp
Wire Wire Line
	12850 7300 12200 7300
Wire Wire Line
	12200 7500 12350 7500
Wire Wire Line
	12350 7500 12350 7700
$Comp
L power:GND #PWR0104
U 1 1 5F65139E
P 12350 8000
F 0 "#PWR0104" H 12350 7750 50  0001 C CNN
F 1 "GND" H 12355 7827 50  0000 C CNN
F 2 "" H 12350 8000 50  0001 C CNN
F 3 "" H 12350 8000 50  0001 C CNN
	1    12350 8000
	1    0    0    -1  
$EndComp
Wire Wire Line
	13150 7300 14350 7300
Wire Wire Line
	14350 4750 14350 7300
$Comp
L Device:D_Zener D7
U 1 1 5F655002
P 9350 4600
F 0 "D7" V 9304 4680 50  0000 L CNN
F 1 "18V" V 9395 4680 50  0000 L CNN
F 2 "Diode_SMD:D_MiniMELF" H 9350 4600 50  0001 C CNN
F 3 "~" H 9350 4600 50  0001 C CNN
	1    9350 4600
	0    1    1    0   
$EndComp
Wire Wire Line
	9350 4450 9350 4350
Connection ~ 9350 4350
Wire Wire Line
	9350 4350 9950 4350
$Comp
L power:GNDA #PWR0105
U 1 1 5F659269
P 9350 4750
F 0 "#PWR0105" H 9350 4500 50  0001 C CNN
F 1 "GNDA" H 9355 4577 50  0000 C CNN
F 2 "" H 9350 4750 50  0001 C CNN
F 3 "" H 9350 4750 50  0001 C CNN
	1    9350 4750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0118
U 1 1 5F65FF20
P 14000 4300
F 0 "#PWR0118" H 14000 4050 50  0001 C CNN
F 1 "GND" H 14005 4127 50  0001 C CNN
F 2 "" H 14000 4300 50  0001 C CNN
F 3 "" H 14000 4300 50  0001 C CNN
	1    14000 4300
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C3
U 1 1 5F65FF26
P 14000 4150
F 0 "C3" H 13950 3700 50  0000 L CNN
F 1 "1000u/25V" H 13800 3800 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D10.0mm_P5.00mm" H 14038 4000 50  0001 C CNN
F 3 "~" H 14000 4150 50  0001 C CNN
	1    14000 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	14000 4000 14000 3950
Connection ~ 14000 3950
Wire Wire Line
	14000 3950 14350 3950
$EndSCHEMATC
